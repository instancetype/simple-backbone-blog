/**
 * Created by instancetype on 7/22/14.
 */
var express = require('express')
  , path = require('path')
  , Bourne = require('bourne')
  , app = express()

var posts = new Bourne('./blogPosts.json')
  , comments = new Bourne('./blogComments.json')

app.configure(function() {
  app.use(express.json())
  app.use(express.static(path.join(__dirname, 'public')))
})

app.get('/posts', function(req, res) {
  posts.find(function(err, results) {
    res.json(results)
  })
})

app.post('/posts', function(req, res) {
  posts.insert(req.body, function(result) {
    res.json(result)
  })
})

app.get('/posts/:id/comments', function(req, res) {
  comments.find(
    { postId: parseInt(req.params.id, 10)}
  , function(err, results) {
      res.json(results)
    }
  )
})

app.post('/posts/:id/comments', function(req, res) {
  comments.insert(req.body, function(err, result) {
    res.json(result)
  })
})

app.get('/*', function(req, res) {
  posts.find(function(err, results) {
    res.render('index.ejs', {posts: JSON.stringify(results)})
  })
})



app.listen(3300)